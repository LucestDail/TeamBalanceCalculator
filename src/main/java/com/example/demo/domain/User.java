package com.example.demo.domain;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Entity
public class User extends BaseTimeEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userSeq;
	
	@Column(columnDefinition = "text", nullable = false)
	private String id;
	
	@Column(columnDefinition = "text", nullable = false)
	private String region;
	
	
	@Builder
	public User(Long userSeq, String id, String region) {
		this.userSeq = userSeq;
		this.id = id;
		this.region = region;
	}	
	
}
