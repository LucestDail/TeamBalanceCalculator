package com.example.demo.webservice;

import com.example.demo.domain.User;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
// ConTroller -> @RequestBody 로 외부에서 데이터 받는 경우엔 기본생성자 + set메소드 통해서만 값이 할
@NoArgsConstructor
public class PostsSearchRequestDto {

	private String id;
	private String region;
	
	@Builder
	public PostsSearchRequestDto(String id, String region) {
		this.id = id;
		this.region = region;
	}
	
	public User toEntity() {
		System.out.println(User.builder().id(id).region(region).build());
		return User.builder()
				.id(id)
				.region(region)
				.build();
	}
	
}
